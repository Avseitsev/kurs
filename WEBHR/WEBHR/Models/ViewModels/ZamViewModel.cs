﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.ViewModels
{
    public class ZamViewModel
    {
        public Zap zap { get; set; }
        public string NEWstr { get; set; }
        public List<string> Sp { get; set; }
        public int OLDid { get; set; }
        public int id2 { get; set; }
        public int NEWcolvo { get; set; }
        public string NEWcoment { get; set; }
    }
}
