﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBHR.Models.SQL;

namespace WEBHR.Models.ViewModels
{
    public class NewZapViewModel
    {
        public HR Hr { get; set; }
        public string Status { get; set; }

        public int Colvo { get; set; }
        public List<string> Sp { get; set; }
        public string str { get; set; }
        public string Coment { get; set; }

    }
}
