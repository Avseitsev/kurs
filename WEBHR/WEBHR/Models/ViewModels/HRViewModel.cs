﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.ViewModels
{
    public class HRViewModel
    {
        public string Mail { get; set; }

        public string NameOrg { get; set; }
        public string NameHR { get; set; }
        public string Name2HR { get; set; }
        public string NamberHR { get; set; }
        public string Posta { get; set; }
        public string UNP { get; set; }

        public string Phone { get; set; }

        public string NamberHR2 { get; set; }
        public string Phone2 { get; set; }
        public string Posta2 { get; set; }
        public string UNP2 { get; set; }
        public string NameOrg2 { get; set; }
        public string NameHR2 { get; set; }
        public string Name2HR2 { get; set; }
    }
}
