﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.ViewModels
{
    public class CarViewModel
    {
        public List<Car> cars { get; set; }
        public int id { get; set; }
        public string Org { get; set; }
        public string God { get; set; }
        public string God2 { get; set; }
        public string Error { get; set; }
    }
}
