﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.ViewModels
{
    public class CarRedViewModel
    {
        public string error { get; set; }
        public int id { get; set; }
        public Car car { get; set; }
        public int carId { get; set; }
        public string Org { get; set; }
        public string God { get; set; }
        public string God2 { get; set; }
    }
}
