﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.ViewModels
{
    public class RegDecViewModel
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public string Error { get; set; }

    }
}
