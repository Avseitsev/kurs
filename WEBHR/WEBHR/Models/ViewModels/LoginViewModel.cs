﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEBHR.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Не указан mail")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [Display(Name = "Пароль")]
        [StringLength(15, MinimumLength = 5)]
        public string Password { get; set; }

        [Display(Name = "Запомнить?")]
        public bool RememberMe { get; set; }

        public string Status { get; set; }

        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }


        public string NameOrg { get; set; }
        public string NameHR { get; set; }
        public string Name2HR { get; set; }
        public string NamberHR { get; set; }
        public string Posta { get; set; }
        public string UNP { get; set; }


        public string Error { get; set; }
        public string ReturnUrl { get; set; }
    }
}
