﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEBHR.Models.ViewModels
{
    public class MailViewModel
    {
        [Required(ErrorMessage = "Не указан mail")]
        [DataType(DataType.EmailAddress)]
        public string MailName { get; set; }
        [StringLength(4, MinimumLength = 4)]
        public string Cod { get; set; }
        public string Miss { get; set; }
    }
}
