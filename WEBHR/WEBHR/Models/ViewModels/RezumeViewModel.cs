﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.ViewModels
{
    public class RezumeViewModel
    {
        public Rezume rezume { get; set; }
        public string Text { get; set; }
        public string Status { get; set; }
        public string Number { get; set; }
        public IFormFile uploadedFile { get; set; }
    }
}
