﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models
{
    public class Zap
    {
        public Zap()
        { }

        public Zap(HR hr, int quantity, string spe, string comment, DateTime data,string status)
        {
            Hr = hr;
            Quantity = quantity;
            SpeList = spe;
            Comment = comment;
            Data = data;
            Status = status;
        }

        public int id { get; set; }
        public HR Hr { get; set; }
        public int Quantity { get; set; }//кол-во
        public string SpeList { get; set; }
        public string Comment { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
    }
}
