﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace WEBHR.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<HR> HR { get; set; }
        public DbSet<Caf> Caf { get; set; }
        public DbSet<Stud> Stud { get; set; }
        public DbSet<Zap> Zap { get; set; }
        public DbSet<Rezume> Rezume { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

    }
}
