﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.SQL
{
    public class spec
    {
        public int id { get; set; }
        public string tip { get; set; }
        public string chifr { get; set; }
        public string vid { get; set; }
        public string fak { get; set; }
        public string kodf { get; set; }
        public string speci { get; set; }
        public string spec_s { get; set; }
        public string name { get; set; }
        public string name1 { get; set; }
        public string name_s1 { get; set; }
        public string name_s2 { get; set; }
        public int IdStudType { get; set; }
        public string spec_dipl { get; set; }
        public string diplom { get; set; }
        public string kvalif { get; set; }
        public string srok { get; set; }
    }
}
