﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.SQL
{
    public class lk_diplom
    {
        public string DEK { get; set; }
        public string GR { get; set; }
        public string Kurs { get; set; }
        [Key]
        public string CHIFR_STD { get; set; }      
        public string chifr { get; set; }
        public string F { get; set; }
        public string I { get; set; }
        public string O { get; set; }
        public string SPEC { get; set; }
    }
}
