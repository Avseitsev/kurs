﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models.SQL
{
    public class dek_diplom
    {
        [Key]
        public int kod1 { get; set; }
        public string kod { get; set; }
        public string name { get; set; }
        public string fak { get; set; }
        public string dekan { get; set; }
        public string dekan_r_p { get; set; }
        public string fak_rp { get; set; }
        public string fak_rp_m { get; set; }
        public string fak_d_p { get; set; }
        public string zam_dek { get; set; }
        public string tel_dek { get; set; }
        public string IdFormaTime { get; set; }
        public string VidTime { get; set; }
        public string fakul { get; set; }
        public string name_1 { get; set; }
    }
}
