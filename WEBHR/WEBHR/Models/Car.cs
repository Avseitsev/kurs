﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace WEBHR.Models
{
    public class Car
    {
        public Car()
        { }
        public Car(OldStud User, string Org, string God, string God2)
        {
            this.User = User;
            this.Org = Org;
            this.God = God;
            this.God2 = God2;
        }

        public int id { get; set; }
        public OldStud User{ get; set; }
        public string Org { get; set; }
        public string God { get; set; }
        public string God2 { get; set; }
    }
}
