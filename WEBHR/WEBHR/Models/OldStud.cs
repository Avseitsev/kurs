﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models
{
    public class OldStud
    {
        public OldStud()
        { }
        public OldStud(string name, string name2, string name3,string sp, string fak, string godV)
        {
            Name = name;
            Name2 = name2;
            Name3 = name3;
            Sp = sp;
            Fak = fak;
            GodV = godV;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Sp { get; set; }
        public string Fak { get; set; }
        public string GodV { get; set; }
    }
}
