﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models
{
    public class Caf
    {
        public Caf()
        { }
        public Caf(User User, string Name)
        {
            this.User = User;
            this.Name = Name;
        }

        public int id { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
    }
}
