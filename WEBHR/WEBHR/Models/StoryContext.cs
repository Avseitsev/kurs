﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace WEBHR.Models
{
    public class StoryContext : DbContext
    {
        public DbSet<OldStud> OldStuds { get; set; }
        public DbSet<Car> Cars { get; set; }
        public StoryContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=oldstudappdb;Trusted_Connection=True;");
        }
    }
}
