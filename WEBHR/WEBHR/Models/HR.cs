﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models
{
    public class HR
    {
        public HR()
        { }

        public HR(User User, string NameOrg, string NameHR, string Name2HR, string NamberHR, string Posta, string UNP, string Phone)
        {
            this.User = User;
            this.NameOrg = NameOrg;
            this.NameHR = NameHR;
            this.Name2HR = Name2HR;
            this.NamberHR = NamberHR;
            this.Posta = Posta;
            this.UNP = UNP;
            this.Phone = Phone;
        }

        public int id { get; set; }
        public User User { get; set; }
        public string NameOrg { get; set; }
        public string NameHR { get; set; }
        public string Name2HR { get; set; }
        public string NamberHR { get; set; }
        public string Posta { get; set; }
        public string UNP { get; set; }
        public string Phone { get; set; }
    }
}
