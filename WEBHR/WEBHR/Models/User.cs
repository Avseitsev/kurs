﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace WEBHR.Models
{
    public class User : IdentityUser, IAuthorizationRequirement
    {
        public User()
        { }
        public User(String Role)//только для клаимов
        {
            this.Role = Role;
        }

        public string Cod { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
