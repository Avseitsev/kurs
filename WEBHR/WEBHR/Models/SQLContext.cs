﻿using Microsoft.EntityFrameworkCore;
using MySQL.Data.EntityFrameworkCore.Extensions;
using WEBHR.Models.SQL;

namespace WEBHR.Models
{
    public class SQLContext : DbContext
    {
        public DbSet<lk_diplom> Lk_diplom { get; set; }
        public DbSet<dek_diplom> Dek_diplom { get; set; }
        public DbSet<spec> Spec { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;UserId=root;Password=root;database=w;SslMode=none;");
        }
    }
}
