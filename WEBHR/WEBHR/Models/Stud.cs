﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models
{
    public class Stud
    {
        public Stud()
        { }
        public Stud(User user, string name, string name2, string name3,string sp)
        {
            User = user;
            Name = name;
            Name2 = name2;
            Name3 = name3;
            Sp = sp;
        }
        public int id { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Sp { get; set; }
    }
}
