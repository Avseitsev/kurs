﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBHR.Models
{
    public class Rezume
    {
        public Rezume()
        { }
        public Rezume(Stud stud, string text, string number, string status,string path)
        {
            Stud = stud;
            Text = text;
            Number = number;
            Status = status;
            Path = path;
        }

        public int id { get; set; }
        public Stud Stud { get; set; }
        public string Text { get; set; }
        public string Number { get; set; }
        public string Status { get; set; }
        public string Path { get; set; }
    }
}
