﻿using System;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace WEBHR.Models
{
    public class RoleHandler : AuthorizationHandler<User>
    {
        ApplicationContext _context;
        public RoleHandler (ApplicationContext context)
        {
            _context = context;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,User requirement)
        {

            try
            {
                if (requirement.Role == _context.Users.FirstOrDefault(x => x.Email == context.User.Identity.Name).Role)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    context.Fail();
                }
            }
            catch
            {
                context.Fail();
            }
            return Task.CompletedTask;
        }
    }
}
