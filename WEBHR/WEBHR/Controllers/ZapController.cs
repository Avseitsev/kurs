﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WEBHR.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace WEBHR.Controllers
{
    [Route("api/[controller]")]
    public class ZapController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public ZapController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet("{id}")]
        public IEnumerable<Zap> Get(int id)
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            if (user.Role == "Cad")
            {
                List<HR> hr = _context.HR.ToList();
                List<Zap> list = _context.Zap.OrderBy(x => x.Data).OrderBy(x => x.Data).ToList();
                return list.Skip(id * 7).Take(7).ToList();
            }
            if (user.Role != "Caf")
            {
                if (user.Role != "Dec")
                {
                    HR hr = _context.HR.FirstOrDefault(x => x.User == user); // если нул значит екать на студа и возрат для него
                    if (hr != null)
                    {
                        try
                        {
                            return _context.Zap.OrderBy(x => x.Data).Where(x => x.Hr == hr).Skip(id * 7).Take(7).ToList();
                        }
                        catch
                        {
                            return _context.Zap.OrderBy(x => x.Data).Where(x => x.Hr == hr).Take(7).ToList();
                        }
                    }
                    else
                    {
                        Stud stud = _context.Stud.FirstOrDefault(x => x.User == user);
                        List<HR> hr2 = _context.HR.ToList();
                        List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();
                        List<Zap> list2 = new List<Zap>();
                        foreach (Zap z in list)
                        {
                           if (z.SpeList != null)
                           {
                               String[] substrings = z.SpeList.Split(',');
                               foreach (string sub in substrings)
                               {
                                   try
                                   {
                                       if (sub.ToUpper() == stud.Sp.ToUpper())
                                       {
                                           list2.Add(z);
                                           break;
                                       }
                                   }
                                   catch
                                   {

                                   }
                               }
                           }
                           else
                           {
                               list2.Add(z);
                           }
                        }

                        try
                        {
                            return list2.Skip(id * 7).Take(7).ToList();
                        }
                        catch
                        {
                            return list2.Take(7).ToList();
                        }

                    }
                }
                else
                {
                    _context.HR.ToList();
                    string caf;
                    List<string> listcaf;
                    using (SQLContext db = new SQLContext())
                    {
                        var lis = db.Dek_diplom.FromSql("SELECT * FROM dek_diplom").ToList();
                        caf = lis.FirstOrDefault(x => x.kod.ToString() == user.Email.Split('-')[0]).name;
                        var lis2 = db.Spec.FromSql("SELECT * FROM spec").ToList();
                        listcaf = lis2.Where(x => x.fak == caf).Select(x => x.chifr).ToList();
                    }

                    List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();
                    List<Zap> list2 = new List<Zap>();
                    foreach (var item in list)
                    {
                        if (item.SpeList != null)
                        {
                            string[] lis = item.SpeList.Split(',');
                            foreach (var item2 in lis)
                            {
                                try
                                {
                                    if (listcaf.FirstOrDefault(x => x == item2) != null)
                                    {
                                        list2.Add(item);
                                        break;
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }
                        else
                        {
                            list2.Add(item);
                        }
                    }
                    try
                    {
                        return list2.Skip(id * 7).Take(7).ToList();
                    }
                    catch
                    {
                        return list2.Take(7).ToList();
                    }
                }
            }
            else
            {
                string b;
                using (SQLContext db = new SQLContext())
                {
                    var lis = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    b = lis.FirstOrDefault(x => x.id.ToString() == user.Email.Split('@')[0]).chifr;
                }



                List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();
                ViewBag.len = 1;
                List<Zap> list2 = new List<Zap>();
                foreach (Zap item in list)
                {
                    try
                    {
                        if (item.SpeList.Split(',').FirstOrDefault(x => x == b) != null)
                        {
                            list2.Add(item);
                        }
                    }
                    catch
                    {

                    }
                }

                return list2.Skip(id * 7).Take(7).ToList();
            }
        }      
    }
}
