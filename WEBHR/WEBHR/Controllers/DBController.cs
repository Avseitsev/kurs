﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WEBHR.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WEBHR.Models.ViewModels;
using System.IO;

namespace WEBHR.Controllers
{
    [Authorize(Policy = "AdminLimit")]
    public class DBController : Controller
    {

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public DBController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;

        }


        public IActionResult USER()
        {
            var users = _context.Users.OrderBy(x => x.Id).ToList();
            return View(users);
        }

        public IActionResult CAF()
        {
            _context.Users.ToList();
            var users = _context.Caf.OrderBy(x => x.id).ToList();
            return View(users);
        }

        public IActionResult HR()
        {
            _context.Users.ToList();
            var users = _context.HR.OrderBy(x => x.id).ToList();
            return View(users);
        }

        public IActionResult STUD()
        {
            _context.Users.ToList();
            var users = _context.Stud.OrderBy(x => x.id).ToList();
            return View(users);
        }

    }
}
