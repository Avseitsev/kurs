﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MailKit.Net.Smtp;
using WEBHR.Models.ViewModels;
using WEBHR.Models;
using Microsoft.EntityFrameworkCore;

namespace WEBHR.Controllers
{
    public class VostController : Controller
    {

        ApplicationContext _context;
        public VostController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult VostI([FromQuery] string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(new MailViewModel { });
        }

        [HttpPost]
        public IActionResult VostI(MailViewModel mail)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            Random rnd = new Random();


            if (!ModelState.IsValid)
            {
                return View(mail);
            }


            try
            {
                User user = _context.Users.FirstOrDefault(x => x.Email == mail.MailName);
                user.Cod = rnd.Next(1000, 9999).ToString();
                _context.Entry(user).State = EntityState.Modified;
                _context.SaveChanges();
                mail.Miss = null;
                //отправка на почту
                var message = new MimeMessage();
                 message.From.Add(new MailboxAddress("avseitsev2@gmail.com"));
                 message.To.Add(new MailboxAddress(mail.MailName));
                 message.Subject = "Test mail";
                message.Body = new TextPart("plain")
                {
                    Text = "Ваш код восстановления пароля для сайта WEBHR - " + user.Cod.ToString() +". Если вы не ожидали это сообщение, то просто игнорируйте его!"
                 };
                
                 using (var client = new SmtpClient())
                 {
                     client.Connect("smtp.gmail.com", 587, false);
                     client.Authenticate("avseitsev2@gmail.com","gfifnhjygfif12345");
                     client.Send(message);
                
                     client.Disconnect(true);
                 }
            }
            catch
            {
                return View(mail);
            }
            return RedirectToAction("Cod", "Vost", mail);
        }

        [HttpGet]
        public IActionResult Cod(MailViewModel mail)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (!ModelState.IsValid)
            {
                return View(mail);
            }

            if (mail.MailName==null)
            {
                mail.Miss = "Введите почту";
                return RedirectToAction("VostI", "Vost", mail);
            }
                User user = _context.Users.FirstOrDefault(x => x.Email == mail.MailName);

            if (user==null)
            {
                mail.Miss = "Нет такой почты";
                return RedirectToAction("VostI", "Vost", mail);
            }

            user = _context.Users.FirstOrDefault(x => x.Email == mail.MailName && x.Cod==mail.Cod);

            if(mail.Cod!=null)
            if (user == null)
            {
                mail.Miss = "Неверный код";
                return View(mail);
            }
            else
            {
                    try
                    {
                        mail.Miss = null;
                        var message = new MimeMessage();
                        message.From.Add(new MailboxAddress("avseitsev2@gmail.com"));
                        message.To.Add(new MailboxAddress(user.Email));
                        message.Subject = "Test mail";
                        message.Body = new TextPart("plain")
                        {
                            Text = "Ваш пароль сайта WEBHR - " + user.Password
                        };

                        using (var client = new SmtpClient())
                        {
                            client.Connect("smtp.gmail.com", 587, false);
                            client.Authenticate("avseitsev2@gmail.com", "gfifnhjygfif12345");
                            client.Send(message);

                            client.Disconnect(true);
                        }
                        mail.Miss = "Пароль выслан на почту!";
                        return View(mail);
                    }
                    catch
                    {
                        mail.Miss = "Что-то пошло не так!";
                        return View(mail);
                    }     
            }

            return View(mail);
        }
    }
}
