﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using WEBHR.Models;
using WEBHR.Models.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace WEBHR.Controllers
{
    [Authorize(Policy = "StudLimit")]
    public class StudController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;
        IHostingEnvironment _appEnvironment;

        public StudController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context, IHostingEnvironment appEnvironment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _appEnvironment = appEnvironment;

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult All()
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            Stud stud = _context.Stud.FirstOrDefault(x => x.User == user);
            List<HR> hr = _context.HR.ToList();

            List<Zap> list = _context.Zap.OrderBy(x => x.Data).Where(x=>x.Hr!=null).ToList();
            List<Zap> list2=new List<Zap>();
            foreach (Zap z in list)
            {
                if (z.Status == "on")
                {
                    if (z.SpeList != null)
                    {
                        String[] substrings = z.SpeList.Split(',');
                        foreach (string sub in substrings)
                        {
                            if (sub.ToUpper() == stud.Sp.ToUpper())
                            {
                                list2.Add(z);
                                break;
                            }
                        }
                    }
                    else
                    {
                        list2.Add(z);
                    }
                }
            }
            return View(list2);
        }

        public IActionResult ForID(int id)
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            Stud stud = _context.Stud.FirstOrDefault(x => x.User == user);
            List<HR> hr = _context.HR.ToList();

            List<Zap> list = _context.Zap.OrderBy(x => x.Data).Where(x => x.Hr != null).ToList();
            List<Zap> list2 = new List<Zap>();
            foreach (Zap z in list)
            {
                if (z.Status == "on")
                {
                    if (z.SpeList != null)
                    {
                        String[] substrings = z.SpeList.Split(',');
                        foreach (string sub in substrings)
                        {
                            if (sub.ToUpper() == stud.Sp.ToUpper())
                            {
                                list2.Add(z);
                                break;
                            }
                        }
                    }
                    else
                    {
                        list2.Add(z);
                    }
                }
            }



            return View(list2[id]);
        }

        [HttpGet]
        public IActionResult Rezume()
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            Stud stud = _context.Stud.FirstOrDefault(x => x.User == user);
            Rezume z = _context.Rezume.FirstOrDefault(x=>x.Stud==stud);
            if(z!=null)
            {
                return View(new RezumeViewModel { rezume = z,Text=z.Text });
            }

            return View(new RezumeViewModel { });
        }

        [HttpPost]
        public async Task<IActionResult> Rezume(RezumeViewModel model)
        {
            string path = null;


            try
            {
                if (model.Number == null)
                {
                    User userl = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                    Stud studl = _context.Stud.FirstOrDefault(x => x.User == userl);
                    model.rezume = _context.Rezume.FirstOrDefault(x => x.Stud == studl);

                    model.Status = "Введите номер!";
                    return View(model);
                }
                if (model.Text == null)
                {
                    User userl = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                    Stud studl = _context.Stud.FirstOrDefault(x => x.User == userl);
                    model.rezume = _context.Rezume.FirstOrDefault(x => x.Stud == studl);

                    model.Status = "Введите страну прожиания/область/город!";
                    return View(model);
                }
                if (model.uploadedFile == null)
                {
                    User userl = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                    Stud studl = _context.Stud.FirstOrDefault(x => x.User == userl);
                    model.rezume = _context.Rezume.FirstOrDefault(x => x.Stud == studl);
                    model.Status = "Надо загрузить резюме!";
                    return View(model);
                }
                else
                {
                    if (model.uploadedFile.Length > 137460980)
                    {
                        User userl = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                        Stud studl = _context.Stud.FirstOrDefault(x => x.User == userl);
                        model.rezume = _context.Rezume.FirstOrDefault(x => x.Stud == studl);
                        model.Status = "У-упс, что-то пошло не так! Возможно файл слишком большой!(>100мб)";
                        return View(model);
                    }
                    Random rnd = new Random();
                    int value = rnd.Next(-9999, 9999);
                    path = "/Files/" + value.ToString() + DateTime.Now.TimeOfDay.Ticks.ToString() + model.uploadedFile.FileName;
                    using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        await model.uploadedFile.CopyToAsync(fileStream);
                    }
                }               


                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                Stud stud = _context.Stud.FirstOrDefault(x => x.User == user);
                Rezume z = new Rezume(stud, model.Text, model.Number, "off", path);//сюда путь запихай

                if (_context.Rezume.FirstOrDefault(x => x.Stud == stud) != null)
                {
                    _context.Rezume.Remove(_context.Rezume.FirstOrDefault(x => x.Stud == stud));
                    _context.SaveChanges();
                }

                _context.Rezume.Add(z);
                _context.SaveChanges();


                return RedirectToAction("Rezume", "Stud");
            }
            catch
            {
                User userl = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                Stud studl = _context.Stud.FirstOrDefault(x => x.User == userl);
                model.rezume = _context.Rezume.FirstOrDefault(x => x.Stud == studl);
                model.Status = "У-упс, что-то пошло не так! Проверьте формат файла";
                return View(model);
            }
        }
        public IActionResult DelRezume()
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            Stud stud = _context.Stud.FirstOrDefault(x => x.User == user);

            if (_context.Rezume.FirstOrDefault(x => x.Stud == stud) != null)
            {
                _context.Rezume.Remove(_context.Rezume.FirstOrDefault(x => x.Stud == stud));
                _context.SaveChanges();
            }

            return RedirectToAction("Rezume", "Stud");
        }
    }
}
