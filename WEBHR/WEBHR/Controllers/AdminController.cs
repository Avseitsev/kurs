﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WEBHR.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WEBHR.Models.ViewModels;
using System.IO;

namespace WEBHR.Controllers
{
    [Authorize(Policy = "AdminLimit")]
    public class AdminController : Controller
    {

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public AdminController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult RegDec(string status)// методы реги кафедры и деканата перепутанны 
        {
            return View(new RegDecViewModel { Error = status });
        }

        public IActionResult RegCaf(string status)
        {
            return View(new RegDecViewModel { Error = status });
        }

        [HttpPost]
        public async Task<IActionResult> RegDec(RegDecViewModel Model)
        {
            string error = null;
            if (Model.Password.Length < 5)
            {
                error = "Пароль короче 5 символов";
            }
            else
            {
                if (Model.Name == null || Model.Password == null)
                {
                    error = "Заполните все поля";
                }
                else
                {
                    using (SQLContext db = new SQLContext())
                    {
                        var list = db.Spec.FromSql("SELECT * FROM spec").ToList();
                        var spec = list.FirstOrDefault(x => x.chifr.ToUpper() == Model.Name.ToUpper());
                        if (spec != null)
                        {
                            int idCaf = spec.id;
                            var cafDB = _context.Caf.FirstOrDefault(x => x.Name == idCaf.ToString());
                            if (cafDB != null)
                            {
                                error = "такая кафедра уже есть!" + idCaf + "@by";
                            }
                            else
                            {
                                try
                                {
                                    User user = new User { Email = idCaf + "@by", UserName = idCaf + "@by", Password = Model.Password, Role = "Caf" };
                                    await _userManager.CreateAsync(user, Model.Password);
                                    _context.Caf.Add(new Caf(_context.Users.FirstOrDefault(x => x.UserName == idCaf + "@by"), idCaf.ToString()));
                                    await _context.SaveChangesAsync();
                                    error = "Успех! Логин:" + idCaf + "@by" + "Пароль:" + Model.Password;
                                }
                                catch
                                {
                                    error = "Что-то пошло не так!";
                                }
                            }
                        }
                        else
                        {
                            error = "Такой кафедры не существует в базе!";
                        }
                    }
                }
            }
            Model.Error = error;
            return View(Model);
        }

        [HttpPost]
        public async Task<IActionResult> RegCaf(RegDecViewModel Model)
        {
            string error = null;
            if (Model.Password.Length < 5)
            {
                error = "Пароль короче 5 символов";
            }
            else
            {
                if (Model.Name == null || Model.Password == null)
                {
                    error = "Заполните все поля";
                }
                else
                {
                    using (SQLContext db = new SQLContext())
                    {
                        var list = db.Dek_diplom.FromSql("SELECT * FROM dek_diplom").ToList();
                        var spec = list.FirstOrDefault(x => x.name.ToUpper() == Model.Name.ToUpper());
                        if (spec != null)
                        {
                            string idDec = spec.kod;
                            var login = idDec + "-" + "dec" + "@by";
                            var cafDB = _context.Users.FirstOrDefault(x => x.Email == login);
                            if (cafDB != null)
                            {
                                error = "Такой деканат уже есть!";
                            }
                            else
                            {
                                try
                                {
                                    User user = new User { Email = login, UserName = login, Password = Model.Password, Role = "Dec" };
                                    await _userManager.CreateAsync(user, Model.Password);
                                    await _context.SaveChangesAsync();
                                    error = "Успех! Логин:" + login + "Пароль:" + Model.Password;
                                }
                                catch
                                {
                                    error = "Что-то пошло не так!";
                                }
                            }
                        }
                        else
                        {
                            error = "Такого деканата не существует в базе!";
                        }
                    }
                }
            }
            Model.Error = error;
            return View(Model);
        }


        public IActionResult DB()
        {
            return View();
        }

    }
}
