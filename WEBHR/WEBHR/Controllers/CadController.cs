﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WEBHR.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WEBHR.Models.ViewModels;
using System.IO;

namespace WEBHR.Controllers
{
    [Authorize(Policy = "CadLimit")]
    public class CadController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public CadController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult HR()
        {
            _context.Users.ToList();
            var users = _context.HR.OrderBy(x => x.id).ToList();
            return View(users);
        }

        public IActionResult HRDel(int id)
        {
            _context.Users.ToList();
            try
            {
                _context.Zap.RemoveRange(_context.Zap.Where(x => x.Hr.id == id));
                _context.SaveChanges();
            }
            catch { }
            try
            {
                var userId = _context.HR.FirstOrDefault(x => x.id == id).User.Id;
                _context.HR.Remove(_context.HR.FirstOrDefault(x => x.id == id));
                _context.SaveChanges();
                try
                {
                    _context.Users.Remove(_context.Users.FirstOrDefault(x => x.Id == userId));
                    _context.SaveChanges();
                }
                catch { }
            }
            catch { }
            return RedirectToAction("Index", "Cad");
        }

        public IActionResult Zav()
        {
            try
            {
                _context.HR.ToList();
                _context.Users.ToList();
                List <Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();
                ViewBag.len = list.Count;
                return View(list);
            }
            catch
            {
                return View();
            }
        }

        public IActionResult ZavForId(int id)
        {
            try
            {
                _context.HR.ToList();
                _context.Users.ToList();
                List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();
                ViewBag.len = list.Count;
                return View(list[id]);
            }
            catch
            {
                return View();
            }
        }

        public IActionResult Mon()
        {
            try
            {
                using (StoryContext db = new StoryContext())
                {
                    List<OldStud> list = db.OldStuds.ToList();
                    return View(list);
                }                       
            }
            catch
            {
                return View();
            }
        }

        public IActionResult CarForId(int id)
        {

            try
            {
                using (StoryContext db = new StoryContext())
                {
                    var user = db.OldStuds.FirstOrDefault(x=>x.Id == id);
                    ViewBag.name = user.Name + " " + user.Name2;
                    List<Car> list = db.Cars.Where(x=>x.User.Id == id).ToList();
                    return View(list);
                }
            }
            catch
            {
                return View();
            }
        }
    }
}
