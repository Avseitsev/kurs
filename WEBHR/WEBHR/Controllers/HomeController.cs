﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WEBHR.Models;
using Microsoft.AspNetCore.Identity;
using WEBHR.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WEBHR.Models.SQL;
using System;
using System.Collections.Generic;
//@if (User.Identity.IsAuthenticated)
namespace WEBHR.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public HomeController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;

        }

        [HttpGet]
        public IActionResult Index(string error)
        {
            // using (SQLContext sql = new SQLContext())  //заполнени таблицы с уже прошлыми выпуск
            // {
            //     var list = sql.Lk_diplom.FromSql("SELECT * FROM lk_diplom").ToList();
            //     var listSP = sql.Spec.FromSql("SELECT * FROM spec").ToList();
            //     using (StoryContext db = new StoryContext())
            //     {
            //         for (int i = 0; i < list.Count; i++)
            //         {
            //             try
            //             {
            //               var sp = listSP.FirstOrDefault(x => x.chifr == list[i].chifr);
            //               OldStud user1 = new OldStud {Name = list[i].F, Name2 = list[i].I, Name3 = list[i].O, Sp = sp.name, Fak = sp.fak, GodV= DateTime.Now.Year.ToString() }; 
            //               // потом для некст год + 1
            //               db.OldStuds.Add(user1);
            //             }
            //             catch
            //             {
            // 
            //             }
            //         }
            //         db.SaveChanges();
            //     }
            // }

            // using (StoryContext db = new StoryContext())
            // {
            //    var user = db.OldStuds.First();
            //     Car car = new Car { Org="ЕПАМ",God="2017",God2="2018", User=user};
            //     Car car2 = new Car { Org = "зип", God = "2015", God2 = "2017", User = user };
            //     // потом для некст год + 1
            //     db.Cars.Add(car);
            //     db.Cars.Add(car2);
            //     db.SaveChanges();
            // }


            ////заполняет кафедры
            // List<string> l = new List<string>();
            // using (SQLContext db = new SQLContext())
            // {
            //     var list = db.Spec.FromSql("SELECT * FROM spec").ToList();
            //     for (int i = 0; i < list.Count; i++)
            //     {
            //         l.Add(list[i].id.ToString());
            //     }
            // }
            ////foreach (string item in l)
            ////{
            //string item = "247";
            // User user3 = new User { Email = item + "@by", UserName = item + "@by", Password = "cafparol", Role = "Caf" };
            // var result = _userManager.CreateAsync(user3, "cafparol");
            // _context.Caf.Add(new Caf(_context.Users.FirstOrDefault(x => x.UserName == item + "@by"), item));
            //    _context.SaveChangesAsync();
            //// }


            //  User user3 = new User { Email = "1-EF@by", UserName = "1-EF@by", Password = "decparol", Role = "Dec" };
            //  var result = await _userManager.CreateAsync(user3, "decparol");
            //
            //  user3 = new User { Email = "2-MTF@by", UserName = "2-MTF@by", Password = "decparol", Role = "Dec" };
            //  result = await _userManager.CreateAsync(user3, "decparol");
            //
            //  user3 = new User { Email = "3-MCF@by", UserName = "3-MCF@by", Password = "decparol", Role = "Dec" };
            //  result = await _userManager.CreateAsync(user3, "decparol");
            //
            //  user3 = new User { Email = "4-GEF@by", UserName = "4-GEF@by", Password = "decparol", Role = "Dec" };
            //  result = await _userManager.CreateAsync(user3, "decparol");
            //
            //  user3 = new User { Email = "5-ZF@by", UserName = "5-ZF@by", Password = "decparol", Role = "Dec" };
            //  result = await _userManager.CreateAsync(user3, "decparol");
            //
            //  user3 = new User { Email = "7-FAIS@by", UserName = "7-FAIS@by", Password = "decparol", Role = "Dec" };
            //  result = await _userManager.CreateAsync(user3, "decparol");
            //
            //  user3 = new User { Email = "8-FDP@by", UserName = "8-FDP@by", Password = "decparol", Role = "Dec" };
            //  result = await _userManager.CreateAsync(user3, "decparol");
            //
            //  user3 = new User { Email = "9-FPK@by", UserName = "9-FPK@by", Password = "decparol", Role = "Dec" };
            //  result = await _userManager.CreateAsync(user3, "decparol");


            // User user3 = new User { Email = "cad@by", UserName = "cad@by", Password = "cadparol", Role = "Cad" };
            // var result = _userManager.CreateAsync(user3, "cadparol");
            // // _context.Caf.Add(new Caf(_context.Users.FirstOrDefault(x => x.UserName == "caf@by"), "ИТ"));
            //  _context.SaveChangesAsync();

            if (User.Identity.IsAuthenticated)
            {
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                try
                {
                    if (user.Role == "Admin")
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    if (user.Role == "Caf")
                    {
                        return RedirectToAction("Index", "Caf");
                    }
                    if (user.Role == "Dec")
                    {
                        return RedirectToAction("Index", "Dec");
                    }
                    if (user.Role == "Cad")
                    {
                        return RedirectToAction("Index", "Cad");
                    }
                    if (user.Role == "HR")
                    {
                        return RedirectToAction("Index", "HR");
                    }
                    if (user.Role == "Stud")
                    {
                        return RedirectToAction("Index", "Stud");
                    }
                }
                catch
                {
                    return RedirectToAction("LogOff", "Home");
                }
            }

            ViewBag.Companies = new SelectList(new string[2] { "Студент", "HR" });

            return View(new LoginViewModel { Error = error });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result =
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
            }

            ViewBag.Companies = new SelectList(new string[2] { "Студент", "HR" });

            return View(model);
        }


        public async Task<IActionResult> LogIn(LoginViewModel model)
        {
            if (_context.Users.FirstOrDefault(x => x.UserName == model.Email) != null)
            {
                return RedirectToAction("Index", "Home", new { error = "Почта занята" });
            }
            if (model.Status == "HR")
            {
                if (ModelState.IsValid && model.NameOrg != null && model.NameHR != null && model.Name2HR != null && model.NamberHR != null && model.Posta != null)
                {
                    if (_context.HR.FirstOrDefault(x => x.NameOrg.ToUpper() == model.NameOrg.ToUpper() && x.NameHR.ToUpper() == model.NameHR.ToUpper() && x.Name2HR.ToUpper() == model.Name2HR.ToUpper()) != null)
                    {
                        return RedirectToAction("Index", "Home", new { error = "Такой представитель данной фирмы уже зарегистрирован" });
                    }
                    if (model.UNP == null) model.UNP = " ";
                    await _userManager.CreateAsync(new User { Email = model.Email, UserName = model.Email, Password = model.Password, Role = "HR" }, model.Password);
                    _context.HR.Add(new HR(_context.Users.FirstOrDefault(x => x.UserName == model.Email), model.NameOrg, model.NameHR, model.Name2HR, model.NamberHR, model.Posta, model.UNP,null));
                    await _context.SaveChangesAsync();
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                    return RedirectToAction("Index", "Home");
                }
            }
            if (model.Status == "Студент")
            {
                if (ModelState.IsValid)
                {
                    string sp="";
                    using (SQLContext db = new SQLContext())
                    {
                        var comps = db.Lk_diplom.FromSql("SELECT * FROM lk_diplom").ToList();
                        lk_diplom d = comps.FirstOrDefault(x => x.I.ToUpper() == model.Name.ToUpper() && x.F.ToUpper() == model.Name2.ToUpper() && x.O.ToUpper() == model.Name3.ToUpper());
                        if (d ==null)
                        {
                            return RedirectToAction("Index", "Home", new { error = "Такого студента нету!" });
                        }
                        else
                        {
                            sp = d.GR;
                            String[] substrings = sp.Split('-');
                            sp = substrings[0];
                        }
                    }
                    Stud stud = _context.Stud.FirstOrDefault(x => x.Name.ToUpper() == model.Name.ToUpper() && x.Name2.ToUpper() == model.Name2.ToUpper() && x.Name3.ToUpper() == model.Name3.ToUpper());
                    if(stud!=null)
                    {
                        return RedirectToAction("Index", "Home", new { error = "Такой студент уже зареган!" });
                    }
                    ///////////////////////////////////
                    await _userManager.CreateAsync(new User { Email = model.Email, UserName = model.Email, Password = model.Password, Role = "Stud" }, model.Password);
                    _context.Stud.Add(new Stud(_context.Users.FirstOrDefault(x => x.UserName == model.Email), model.Name,model.Name2,model.Name3,sp));
                    await _context.SaveChangesAsync();
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                    //////////////////////////////////////////
                    return RedirectToAction("Index", "Home");

                }
            }
            return RedirectToAction("Index", "Home", new { error = "Пустые или некорректные поля ввода" });
        }


        public IActionResult I()
        {
            return RedirectToAction("Index", "Home");
        }

            public async Task<IActionResult> LogOff()
       {
           await _signInManager.SignOutAsync();
           return RedirectToAction("Index", "Home");
       }
    }
}
