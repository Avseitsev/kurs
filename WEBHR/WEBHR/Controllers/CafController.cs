﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WEBHR.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WEBHR.Models.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEBHR.Controllers
{
    [Authorize(Policy = "CafLimit")]
    public class CafController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public CafController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Zav()
        {
            try
            {
                _context.HR.ToList();
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                string b;
                using (SQLContext db = new SQLContext())
                {
                    var lis = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    b = lis.FirstOrDefault(x => x.id.ToString() == user.Email.Split('@')[0]).chifr;
                }



                List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();

                List<Zap> list2 = new List<Zap>();
                foreach (Zap item in list)
                {
                    try
                    {
                        if (item.SpeList.Split(',').FirstOrDefault(x => x == b) != null)
                        {

                            list2.Add(item);
                        }
                    }
                    catch
                    {

                    }
                }


                ViewBag.len = list2.Count;
                return View(list2);
            }
            catch
            {
                return View();
            }
        }


        public IActionResult ZavForId(int id)
        {
            try
            {
                _context.HR.ToList();
                _context.Users.ToList();
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                string b;
                using (SQLContext db = new SQLContext())
                {
                    var lis = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    b = lis.FirstOrDefault(x => x.id.ToString() == user.Email.Split('@')[0]).chifr;
                }



                List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();
                ViewBag.len = 1;
                List<Zap> list2 = new List<Zap>();
                foreach (Zap item in list)
                {
                    try
                    {
                        if (item.SpeList.Split(',').FirstOrDefault(x => x == b) != null)
                        {
                            list2.Add(item);
                        }
                    }
                    catch
                    {

                    }
                }

                return View(list2[id]);
            }
            catch
            {
                return View();
            }
        }

        public IActionResult Zac(int id)
        {
            try
            {
                _context.Zap.FirstOrDefault(x => x.id == id).Status = "off";
                _context.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("Zav", "Caf");
        }

        public IActionResult Open(int id)
        {
            try
            {
                _context.Zap.FirstOrDefault(x => x.id == id).Status = "on";
                _context.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("Zav", "Caf");
        }

        public IActionResult Rez()
        {
            _context.HR.ToList();
            _context.Users.ToList();
            _context.Stud.ToList();
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            string b;
            using (SQLContext db = new SQLContext())
            {
                var lis = db.Spec.FromSql("SELECT * FROM spec").ToList();
                b = lis.FirstOrDefault(x => x.id.ToString() == user.Email.Split('@')[0]).chifr;
            }

            List<Rezume> list = _context.Rezume.Where(x => x.Stud.Sp == b && x.Status == "off").ToList();

            return View(list);
        }

        public IActionResult YES(int id)
        {
            try
            {
                _context.Rezume.FirstOrDefault(x => x.id == id).Status = "on";
                _context.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("Rez", "Caf");
        }

        public IActionResult NO(int id)
        {
            _context.Rezume.Remove(_context.Rezume.FirstOrDefault(x => x.id == id));
            _context.SaveChanges();
            return RedirectToAction("Rez", "Caf");
        }
        public IActionResult NO2(int id)
        {
            _context.Rezume.Remove(_context.Rezume.FirstOrDefault(x => x.id == id));
            _context.SaveChanges();
            return RedirectToAction("OldRez", "Caf");
        }


        public IActionResult OldRez()
        {
            _context.HR.ToList();
            _context.Users.ToList();
            _context.Stud.ToList();
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            string b;
            using (SQLContext db = new SQLContext())
            {
                var lis = db.Spec.FromSql("SELECT * FROM spec").ToList();
                b = lis.FirstOrDefault(x => x.id.ToString() == user.Email.Split('@')[0]).chifr;
            }

            List<Rezume> list = _context.Rezume.Where(x => x.Stud.Sp == b && x.Status == "on").ToList();

            return View(list);
        }

        public IActionResult Mon()
        {
            _context.HR.ToList();
            _context.Users.ToList();
            _context.Stud.ToList();
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            try
            {
                using (SQLContext sql = new SQLContext())
                {
                    var listSpec = sql.Spec.FromSql("SELECT * FROM spec").ToList();
                    var spec = listSpec.FirstOrDefault(x => x.id.ToString() == user.Email.Split('@')[0]).name;
                    using (StoryContext db = new StoryContext())
                    {
                        List<OldStud> list = db.OldStuds.Where(x=>x.Sp == spec).ToList();
                        return View(list);
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        public IActionResult CarForId(int id, string error)
        {
            try
            {
                using (StoryContext db = new StoryContext())
                {
                    var user = db.OldStuds.FirstOrDefault(x => x.Id == id);
                    ViewBag.name = user.Name + " " + user.Name2;
                    List<Car> list = db.Cars.Where(x => x.User.Id == id).ToList();
                    return View(new CarViewModel { cars = list, id = id, Error = error });
                }
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public IActionResult NewCar(CarViewModel model)
        {
            try
            {
                if (model.Org == null || model.Org.Length < 2 ) { return RedirectToAction("CarForId", "Caf", new { id = model.id, error = "укажите название организации" }); }
                if (model.God == null || model.God.Length < 4) { return RedirectToAction("CarForId", "Caf", new { id = model.id, error = "неверно указан год начала" }); }
                try
                {
                    if (Int32.Parse(model.God2) < 0 ||  Int32.Parse(model.God)<0) { return RedirectToAction("CarForId", "Caf", new { id = model.id, error = "отрицательный год?" }); }
                }
                catch
                {
                    if (model.God2 != null)
                        return RedirectToAction("CarForId", "Caf", new { id = model.id, error = "невалидный год!" }); 
                }
                try
                {
                    if (!(model.God2.Length == 4 || model.God2.Length == 0)) { return RedirectToAction("CarForId", "Caf", new { id = model.id, error = "неверно указан год конца" }); }
                    if (Int32.Parse(model.God2) < Int32.Parse(model.God)) { return RedirectToAction("CarForId", "Caf", new { id = model.id, error = "год окончания не может быть меньше года начала" }); }
                    
                }
                catch
                {

                }
                using (StoryContext db = new StoryContext())
                {
                    var user = db.OldStuds.FirstOrDefault(x => x.Id == model.id);
                    db.Cars.Add(new Car(user,model.Org,model.God,model.God2));
                    db.SaveChanges();
                }
            }
            catch
            {
                return RedirectToAction("CarForId", "Caf", new { id = model.id, error = "что-то пошло не так!" });
            }
            return RedirectToAction("CarForId", "Caf", new { id = model.id });
        }

        public IActionResult CarRemove(int id)
        {
            try
            {
                using (StoryContext db = new StoryContext())
                {
                    db.OldStuds.ToList();
                    var car = db.Cars.FirstOrDefault(x => x.id == id);
                    var userId = car.User.Id;
                     db.Cars.Remove(car);
                    db.SaveChanges();
                    return RedirectToAction("CarForId", "Caf", new { id = userId });
                }
            }
            catch
            {
                return RedirectToAction("Mon", "Caf" );
            }
        }

        public IActionResult CarRed(int id, string error)
        {
            try
            {
                using (StoryContext db = new StoryContext())
                {
                    db.OldStuds.ToList();
                    var car = db.Cars.FirstOrDefault(x => x.id == id);
                    var userId = car.User.Id;
                    return View(new CarRedViewModel { id = userId, car = car, error = error});
                }
            }
            catch
            {
                return RedirectToAction("Mon", "Caf");
            }
        }

        [HttpPost]
        public IActionResult CarRed(CarRedViewModel model)
        {
            try
            {
                if (model.Org == null || model.Org.Length < 2) { return RedirectToAction("CarRed", "Caf", new { id = model.carId, error = "укажите название организации" }); }
                if (model.God == null || model.God.Length < 4) { return RedirectToAction("CarRed", "Caf", new { id = model.carId, error = "неверно указан год начала" }); }
                try
                {
                    if (Int32.Parse(model.God2) < 0 || Int32.Parse(model.God) < 0) { return RedirectToAction("CarRed", "Caf", new { id = model.carId, error = "отрицательный год?" }); }
                }
                catch
                {
                    if (model.God2 != null)
                        return RedirectToAction("CarRed", "Caf", new { id = model.carId, error = "невалидный год!" });
                }
                try
                {
                    if (!(model.God2.Length == 4 || model.God2.Length == 0)) { return RedirectToAction("CarRed", "Caf", new { id = model.carId, error = "неверно указан год конца" }); }
                    if (Int32.Parse(model.God2) < Int32.Parse(model.God)) { return RedirectToAction("CarRed", "Caf", new { id = model.carId, error = "год окончания не может быть меньше года начала" }); }
                }
                catch
                {

                }
                using (StoryContext db = new StoryContext())
                {
                    db.OldStuds.ToList();
                    var car = db.Cars.FirstOrDefault(x => x.id == model.carId);
                    car.Org = model.Org;
                    car.God = model.God;
                    car.God2 = model.God2;
                    db.Cars.Update(car);
                    db.SaveChanges();
                    return RedirectToAction("CarForId", "Caf", new { id = model.id });
                }
            }
            catch
            {
                return RedirectToAction("CarRed", "Caf", new { id = model.carId, error = "что-то пошло не так!" });
            }      
        }
    }
}
