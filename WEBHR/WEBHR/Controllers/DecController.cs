﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using WEBHR.Models;
using Microsoft.EntityFrameworkCore;
using WEBHR.Models.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEBHR.Controllers
{
    [Authorize(Policy = "DecLimit")]
    public class DecController : Controller
    {

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public DecController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Zav()
        {
            try
            {
                _context.HR.ToList();
                _context.Users.ToList();
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                string caf;
                List<string> listcaf;
               using (SQLContext db = new SQLContext())
               {
                   var lis = db.Dek_diplom.FromSql("SELECT * FROM dek_diplom").ToList();
                   caf = lis.FirstOrDefault(x => x.kod.ToString() == user.Email.Split('-')[0]).name;
                   var lis2 = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    listcaf = lis2.Where(x=>x.fak==caf).Select(x=>x.chifr).ToList();
                }

               List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();              
               List<Zap> list2 = new List<Zap>();
                foreach(var item in list)
                {
                    if (item.SpeList != null)
                    {
                        string[] lis = item.SpeList.Split(',');
                        foreach (var item2 in lis)
                        {
                            if (listcaf.FirstOrDefault(x => x == item2) != null)
                            {
                                list2.Add(item);
                                break;
                            }
                        }
                    }
                    else
                    {
                        list2.Add(item);
                    }
                }
              
                ViewBag.len = list2.Count;
               return View(list2);
            }
            catch
            {
                return View();
            }
        }

        public IActionResult ZavForId(int id)
        {
            try
            {
                _context.HR.ToList();
                _context.Users.ToList();
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                string caf;
                List<string> listcaf;
                using (SQLContext db = new SQLContext())
                {
                    var lis = db.Dek_diplom.FromSql("SELECT * FROM dek_diplom").ToList();
                    caf = lis.FirstOrDefault(x => x.kod.ToString() == user.Email.Split('-')[0]).name;
                    var lis2 = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    listcaf = lis2.Where(x => x.fak == caf).Select(x => x.chifr).ToList();
                }

                List<Zap> list = _context.Zap.OrderBy(x => x.Data).ToList();
                List<Zap> list2 = new List<Zap>();
                foreach (var item in list)
                {
                    if (item.SpeList != null)
                    {
                        string[] lis = item.SpeList.Split(',');
                        foreach (var item2 in lis)
                        {
                            if (listcaf.FirstOrDefault(x => x == item2) != null)
                            {
                                list2.Add(item);
                                break;
                            }
                        }
                    }
                    else
                    {
                        list2.Add(item);
                    }
                }

                return View(list2[id]);
            }
            catch
            {
                return View();
            }
        }

        public IActionResult Zac(int id)
        {
            try
            {
                _context.Zap.FirstOrDefault(x => x.id == id).Status = "off";
                _context.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("Zav", "Dec");
        }

        public IActionResult Open(int id)
        {
            try
            {
                _context.Zap.FirstOrDefault(x => x.id == id).Status = "on";
                _context.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("Zav", "Dec");
        }

        public IActionResult Rez()
        {
            try
            {
                _context.Stud.ToList();
                _context.Users.ToList();
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                string caf;
                List<string> listcaf;
                using (SQLContext db = new SQLContext())
                {
                    var lis = db.Dek_diplom.FromSql("SELECT * FROM dek_diplom").ToList();
                    caf = lis.FirstOrDefault(x => x.kod.ToString() == user.Email.Split('-')[0]).name;
                    var lis2 = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    listcaf = lis2.Where(x => x.fak == caf).Select(x => x.chifr).ToList();
                }

                List<Rezume> list = _context.Rezume.Where(x => x.Status == "off").ToList();
                List<Rezume> list2 = new List<Rezume>();
                foreach (var item in list)
                {
                    if (listcaf.FirstOrDefault(x => x == item.Stud.Sp) != null)
                    {
                        list2.Add(item);
                    }
                }

                return View(list2);
            }
            catch
            {
                return View();
            }
        }

        public IActionResult YES(int id)
        {
            try
            {
                _context.Rezume.FirstOrDefault(x => x.id == id).Status = "on";
                _context.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("Rez", "Dec");
        }

        public IActionResult NO(int id)
        {
            _context.Rezume.Remove(_context.Rezume.FirstOrDefault(x => x.id == id));
            _context.SaveChanges();
            return RedirectToAction("Rez", "Dec");
        }
        public IActionResult NO2(int id)
        {
            _context.Rezume.Remove(_context.Rezume.FirstOrDefault(x => x.id == id));
            _context.SaveChanges();
            return RedirectToAction("OldRez", "Dec");
        }

        public IActionResult OldRez()
        {
            try
            {
                _context.Stud.ToList();
                _context.Users.ToList();
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                string caf;
                List<string> listcaf;
                using (SQLContext db = new SQLContext())
                {
                    var lis = db.Dek_diplom.FromSql("SELECT * FROM dek_diplom").ToList();
                    caf = lis.FirstOrDefault(x => x.kod.ToString() == user.Email.Split('-')[0]).name;
                    var lis2 = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    listcaf = lis2.Where(x => x.fak == caf).Select(x => x.chifr).ToList();
                }

                List<Rezume> list = _context.Rezume.Where(x => x.Status == "on").OrderBy(x=>x.Stud.Sp).ToList();
                List<Rezume> list2 = new List<Rezume>();
                foreach (var item in list)
                {
                    if (listcaf.FirstOrDefault(x => x == item.Stud.Sp) != null)
                    {
                        list2.Add(item);
                    }
                }

                return View(list2);
            }
            catch
            {
                return View();
            }
        }

        public IActionResult Mon()
        {
            _context.HR.ToList();
            _context.Users.ToList();
            _context.Stud.ToList();
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            try
            {
                using (SQLContext sql = new SQLContext())
                {
                    var lis = sql.Dek_diplom.FromSql("SELECT * FROM dek_diplom").ToList();
                   var dec = lis.FirstOrDefault(x => x.kod.ToString() == user.Email.Split('-')[0]).name;
                    using (StoryContext db = new StoryContext())
                    {
                        List<OldStud> list = db.OldStuds.Where(x=>x.Fak == dec).ToList();
                        return View(list);
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        public IActionResult CarForId(int id, string error)
        {
            try
            {
                using (StoryContext db = new StoryContext())
                {
                    var user = db.OldStuds.FirstOrDefault(x => x.Id == id);
                    ViewBag.name = user.Name + " " + user.Name2;
                    List<Car> list = db.Cars.Where(x => x.User.Id == id).ToList();
                    return View(new CarViewModel { cars = list, id = id, Error = error });
                }
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public IActionResult NewCar(CarViewModel model)
        {
            try
            {
                if (model.Org == null || model.Org.Length < 2) { return RedirectToAction("CarForId", "Dec", new { id = model.id, error = "укажите название организации" }); }
                if (model.God == null || model.God.Length < 4) { return RedirectToAction("CarForId", "Dec", new { id = model.id, error = "неверно указан год начала" }); }
                try
                {
                    if (Int32.Parse(model.God2) < 0 || Int32.Parse(model.God) < 0) { return RedirectToAction("CarForId", "Dec", new { id = model.id, error = "отрицательный год?" }); }
                }
                catch
                {
                    if (model.God2 != null)
                        return RedirectToAction("CarForId", "Dec", new { id = model.id, error = "невалидный год!" });
                }
                try
                {
                    if (!(model.God2.Length == 4 || model.God2.Length == 0)) { return RedirectToAction("CarForId", "Dec", new { id = model.id, error = "неверно указан год конца" }); }
                    if (Int32.Parse(model.God2) < Int32.Parse(model.God)) { return RedirectToAction("CarForId", "Dec", new { id = model.id, error = "год окончания не может быть меньше года начала" }); }

                }
                catch
                {

                }
                using (StoryContext db = new StoryContext())
                {
                    var user = db.OldStuds.FirstOrDefault(x => x.Id == model.id);
                    db.Cars.Add(new Car(user, model.Org, model.God, model.God2));
                    db.SaveChanges();
                }
            }
            catch
            {
                return RedirectToAction("CarForId", "Dec", new { id = model.id, error = "что-то пошло не так!" });
            }
            return RedirectToAction("CarForId", "Dec", new { id = model.id });
        }

        public IActionResult CarRemove(int id)
        {
            try
            {
                using (StoryContext db = new StoryContext())
                {
                    db.OldStuds.ToList();
                    var car = db.Cars.FirstOrDefault(x => x.id == id);
                    var userId = car.User.Id;
                    db.Cars.Remove(car);
                    db.SaveChanges();
                    return RedirectToAction("CarForId", "Dec", new { id = userId });
                }
            }
            catch
            {
                return RedirectToAction("Mon", "Dec");
            }
        }

        public IActionResult CarRed(int id, string error)
        {
            try
            {
                using (StoryContext db = new StoryContext())
                {
                    db.OldStuds.ToList();
                    var car = db.Cars.FirstOrDefault(x => x.id == id);
                    var userId = car.User.Id;
                    return View(new CarRedViewModel { id = userId, car = car, error = error });
                }
            }
            catch
            {
                return RedirectToAction("Mon", "Dec");
            }
        }

        [HttpPost]
        public IActionResult CarRed(CarRedViewModel model)
        {
            try
            {
                if (model.Org == null || model.Org.Length < 2) { return RedirectToAction("CarRed", "Dec", new { id = model.carId, error = "укажите название организации" }); }
                if (model.God == null || model.God.Length < 4) { return RedirectToAction("CarRed", "Dec", new { id = model.carId, error = "неверно указан год начала" }); }
                try
                {
                    if (Int32.Parse(model.God2) < 0 || Int32.Parse(model.God) < 0) { return RedirectToAction("CarRed", "Dec", new { id = model.carId, error = "отрицательный год?" }); }
                }
                catch
                {
                    if (model.God2 != null)
                        return RedirectToAction("CarRed", "Dec", new { id = model.carId, error = "невалидный год!" });
                }
                try
                {
                    if (!(model.God2.Length == 4 || model.God2.Length == 0)) { return RedirectToAction("CarRed", "Dec", new { id = model.carId, error = "неверно указан год конца" }); }
                    if (Int32.Parse(model.God2) < Int32.Parse(model.God)) { return RedirectToAction("CarRed", "Dec", new { id = model.carId, error = "год окончания не может быть меньше года начала" }); }
                }
                catch
                {

                }
                using (StoryContext db = new StoryContext())
                {
                    db.OldStuds.ToList();
                    var car = db.Cars.FirstOrDefault(x => x.id == model.carId);
                    car.Org = model.Org;
                    car.God = model.God;
                    car.God2 = model.God2;
                    db.Cars.Update(car);
                    db.SaveChanges();
                    return RedirectToAction("CarForId", "Dec", new { id = model.id });
                }
            }
            catch
            {
                return RedirectToAction("CarRed", "Dec", new { id = model.carId, error = "что-то пошло не так!" });
            }
        }
    }
}
