﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WEBHR.Models.ViewModels;
using WEBHR.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;

namespace WEBHR.Controllers
{
    [Authorize(Policy = "HRLimit")]
    public class HRController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        ApplicationContext _context;

        public HRController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;

        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult NewZap(string status)
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            HR hr = _context.HR.FirstOrDefault(x => x.User == user);
            List<string> l=new List<string>();
            using (SQLContext db = new SQLContext())
            {
               var list = db.Spec.FromSql("SELECT * FROM spec").ToList();
                for (int i = 0; i < list.Count; i++)
                {
                    l.Add(list[i].chifr);
                }
            }

            return View(new NewZapViewModel { Hr=hr,Status=status,Sp=l});
        }
        [HttpPost]
        public async Task<IActionResult> NewZap(NewZapViewModel Model)
        {
            if (Model.Colvo <= 0)
            {
                return RedirectToAction("NewZap", "HR", new { status = "Надо запросить хотя бы 1 человека" });
            }
            DateTime s = DateTime.Now.ToUniversalTime();
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            HR hr = _context.HR.FirstOrDefault(x => x.User == user);

           _context.Zap.Add(new Zap(hr, Model.Colvo, Model.str, Model.Coment, s,"on"));
           await _context.SaveChangesAsync();
            //добавь логику добавления в бд 


            return RedirectToAction("NewZap", "HR", new { status = "Заявка принята" });
        }

        public IActionResult I()
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            HR hr = _context.HR.FirstOrDefault(x => x.User == user);
            return View(new HRViewModel{Mail = User.Identity.Name,NameHR=hr.NameHR,NameOrg=hr.NameOrg,Name2HR=hr.Name2HR,NamberHR=hr.NamberHR,Posta=hr.Posta,UNP=hr.UNP,Phone=hr.Phone});
        }


        public async Task<IActionResult> HRChange(HRViewModel model)
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            HR hr = _context.HR.FirstOrDefault(x => x.User == user);

            if(model.Phone2!=null)
                hr.Phone = model.Phone2;
            if (model.Posta2 != null)
                hr.Posta = model.Posta2;
            if (model.NamberHR2 != null)
                hr.NamberHR = model.NamberHR2;
            if (model.UNP2 != null)
                hr.UNP = model.UNP2;
            if (model.Name2HR2 != null)
                hr.Name2HR = model.Name2HR2;
            if (model.NameHR2 != null)
                hr.NameHR = model.NameHR2;
            if (model.NameOrg2 != null)
                hr.NameOrg = model.NameOrg2;

        await _context.SaveChangesAsync();
            return RedirectToAction("I", "HR");
        }


        public IActionResult Scan()
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            HR hr = _context.HR.FirstOrDefault(x => x.User == user);

           List<Zap> list = _context.Zap.OrderBy(x=>x.Data).Where(x=>x.Hr == hr).ToList();
            ViewBag.len = list.Count();
            return View(list);
        }

        public IActionResult Zamena(int id)
        {
            try
            {
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                HR hr = _context.HR.FirstOrDefault(x => x.User == user);
                Zap ll = _context.Zap.OrderBy(x => x.Data).Where(x => x.Hr == hr).ToList()[id];
                List<string> l = new List<string>();
                using (SQLContext db = new SQLContext())
                {
                    var list = db.Spec.FromSql("SELECT * FROM spec").ToList();
                    for (int i = 0; i < list.Count; i++)
                    {
                        l.Add(list[i].chifr);
                    }
                }
                return View(new ZamViewModel { zap=ll,Sp=l,OLDid=id,NEWcoment=ll.Comment});
            }
            catch
            {
                return RedirectToAction("Scan", "HR");
            }
          
        }

        public IActionResult Del(int id)
        {
            try
            {
                 _context.Zap.Remove(_context.Zap.FirstOrDefault(x => x.id == id));
                 _context.SaveChanges();
            }
            catch { }
            return RedirectToAction("Scan", "HR");
        }

        public IActionResult End(int id)
        {
            try
            {
                Zap zap = _context.Zap.FirstOrDefault(x => x.id == id);
                zap.Status = "off";
                _context.SaveChanges();
            }
            catch { }
            return RedirectToAction("Scan", "HR");
        }

        public IActionResult Con(ZamViewModel model)
        {
            try
            {
                User user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                HR hr = _context.HR.FirstOrDefault(x => x.User == user);
                Zap ll = _context.Zap.OrderBy(x => x.Data).Where(x => x.Hr == hr).ToList()[model.id2];
                if (model.NEWcolvo>0)
                {
                    ll.Quantity = model.NEWcolvo;
                }
                    ll.SpeList = model.NEWstr;
                ll.Comment = model.NEWcoment;
                _context.SaveChanges();
                return RedirectToAction("Zamena", "HR",new {id=model.id2});
            }
            catch
            {
                return RedirectToAction("Scan", "HR");
            }
        }

        public IActionResult AllRezume()
        {
            List<string> l = new List<string>();
            using (SQLContext db = new SQLContext())
            {
                var list = db.Spec.FromSql("SELECT * FROM spec").ToList();

                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].name1 != null)
                    {
                        l.Add(list[i].name1);
                    }
                    else
                    {
                        l.Add(list[i].name);
                    }
                }
            }

            return View(l);
        }

        public IActionResult RezumeList(int id)
        {

            List<User> user = _context.Users.ToList();
            List <Stud> stud = _context.Stud.ToList();

            List<Rezume> l = new List<Rezume>();
            using (SQLContext db = new SQLContext())
            {
                var list = db.Spec.FromSql("SELECT * FROM spec").ToList();
                l = _context.Rezume.Where(x=>x.Stud.Sp== list[id].chifr && x.Status=="on").ToList();
            }

            return View(l);
        }

    }
}
